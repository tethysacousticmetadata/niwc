% array2tethys(server, arrays, range)
% Given one or more NIWC array filenames, import them to Tethys
% Assumes all array configurationsa are for a specific Navy range, e.g.
% PMRF.
function array2tethys(server, arrays, range)

% Sanity checks for user arguments

if ~isa(server, 'dbxml.Queries')
    error('server must be a Tethys server handle (create with dbInit)');
end

if ischar(arrays)
    arrays = {arrays};  % convert to cell
elseif ~ iscell(arrays)
    error('Expecting cell array of array files to convert');
end

import nilus.MarshalXML;
import nilus.Ensemble;
import nilus.Helper;
import nilus.UnitGroup;

% stream class to which XML will be written
import java.io.ByteArrayOutputStream;


m = MarshalXML();
helper = Helper();

for fidx = 1:length(arrays)
    fprintf('Generating Tethys ensemble from %s\n', arrays{fidx})
    % Find name of file, assume filename starts with range name
    [~, fname , ~] = fileparts(arrays{fidx});

    ary = load(arrays{fidx});
    
    for aidx = 1:length(ary)
        % Each array entry references a primary hydrophone and a
        % list of secondary hydrophones.
        
        ensemble = Ensemble();
        % Create the required fields of the top-level of the Detections
        % schema.  Any optional fields will need to be created separately
        helper.createRequiredElements(ensemble);

        secondaries = ary.array_struct(aidx).slave;
        secondary = sprintf('%d-', secondaries);
        secondary(end) = [];  % Remove last _
        name = sprintf('%s_m%d_s%s', range, ...
            ary.array_struct(aidx).master, ...
            secondary);
        ensemble.setName(name);
        
        % Add deployments associated with ensemble
        units = ensemble.getUnit();  % Get list of unit groups (empty)
        
        % primary hydrophone
        primary = UnitGroup();
        primary.setUnitId(helper.toXsInteger(1));  % primary always 1
        primary.setDeploymentId(sprintf(...
            '%s_%d', range, ary.array_struct(aidx).master));
        units.add(primary);
        
        % secondary hydrophones 
        for secunit = 1:length(secondaries)
            secondary = UnitGroup();
            secondary.setUnitId(helper.toXsInteger(secunit+1));  % 2, 3, up to last
            secondary.setDeploymentId(sprintf(...
                '%s_%d', range, secondaries(secunit)));
            units.add(secondary);
        end
               
        xml = ByteArrayOutputStream();
        m.marshal(ensemble, xml);
        fprintf(char(xml));  % Convert to Matlab string & print
    end

            
    
    1;
end
   