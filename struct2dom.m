% struct2dom(s, dom)
% Given structure s, add it to a getAny list
function struct2dom(s, any)

fields = fieldnames(s);  % Assume field names valid XML names
% Populate the elements
for fldidx = 1:length(fields)
    val = s.(fields{fldidx});
    if isnumeric(val)
        val = num2str(val);
    end
    helper.AddAnyElement(any, fields{fldidx}, val);

end
