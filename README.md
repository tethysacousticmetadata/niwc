# NIWC

Tools for exporting marine mammal detections, classifications, and localizations from cabled hydrophone observatories to Tethys.  This has been developed for the tool chain developed by NIWC Pacific.  While the input data structures are NIWC specifc, the code provides examples of how to use the Nilus export system.