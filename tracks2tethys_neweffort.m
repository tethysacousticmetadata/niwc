function tracks2tethys_neweffort(q, effortfiles, trackfiles)
%
% Given a server and a set of NIWC files with track data,
% convert to Tethys format.

%This version of the code is using the effort files made using
%effortanalysis_usingdetfiles.m
%See Lombard notes from 11/6-7/2019 for more details
%
% server - server handle from dbInit
% effortfiles - Specifies the tracking effort.
%   nname of a file or cell array of files to process
% trackfiles - Specifies the tracks themselves
%   name of a file or cell array of files to process
%
% Effort and track files are pairs.  Each effort file specifies where
% tracking was done in 10 min blocks.  
%
% Each track file contains an array of tracks in vTkC.  Currently used
% fields of vTkC:
%   nest - Structure containing date and position
%   validation - Validation coding
% Each effort file contains an effC field which 

debug = true;

workingdir = tempdir();  % Location to write XML files

% Only retain validated tracks
validationfilter = [1 2];

% Verify inputs
narginchk(3,3);  
if ischar(trackfiles)   % Track files to process
    trackfiles = {trackfiles};  % string->cell array for uniform processing
else
    % Check for cell array, don't bother verifying cell of strings
    assert(iscell(trackfiles), 'niwc:BadInput', ...
        'trackfiles must be a string or cell array of strings');
end
trackfilesN = length(trackfiles);%number of files.  Should just be 1

if ischar(effortfiles)
    effortfiles = {effortfiles};  % string->cell array for uniform processing
else
    % Check for cell array, don't bother verifying cell of strings
    assert(iscell(effortfiles), 'niwc:BadInput', ...
        'effortfiles must be a string or cell array of strings');
end



import java.lang.Double;   % todo:  remove me when Helper recompiled
import nilus.MarshalXML;
import nilus.Localize;
import nilus.Helper;
import nilus.AlgorithmType;
import nilus.LocalizationType;
import nilus.ObjectFactory;

% The following objects help us construct the XML
m = MarshalXML();  % Converts from Java objects to XML
% Auxiliary functions such as converting timestamps, populating elements
helper = Helper();
% Used for creating objects that cannot be easily handled by helper
% or directly invoiked.  Examples include nested classes.  For example,
% the localization CorrelationData element which has a type of
% NIWCCorrelationData can have a list of 
factory = ObjectFactory();



for fidx = 1:length(trackfilesN)
    fprintf('Processing %s (%d of %d)\n', trackfiles{fidx}, ...
        fidx, trackfilesN);
    % Load tracks and effort
    trk = load(trackfiles{fidx});
    eff = load(effortfiles{fidx});  % only load what we need
    effort = eff.spreadsheettimes; % Start and end of effort periods
%     if ~ issorted(effort)
%         effort = sort(effort);
%     end
    
    % Preprocess effort into contiguous regions
%     effort = datetime(effort, 'ConvertFrom', 'datenum');
%     delta_effort = diff(effort);
%     delta_effort.Format = 'hh:mm:ss.SSS';  %for debug display
%     
%     % Locate breaks in effort > effblock_m minutes apart
%     % Effort is expected to be effblock_m minutes apart as only start
%     % times were recorded.  A tolerance of efftol_s is allowed, that
%     % is blocks less than efftol_s later than expected will not be
%     % considered a separate effort.
%     effblock_m = duration(0, 10, 0);
%     efftol_s = duration(0, 0, 5);
%     gaps = find(delta_effort > effblock_m + efftol_s);
%     gapsN = length(gaps);
%     
%     % Process efforts into blocks
%     effregions = NaT(gapsN+1, 2);  % preallocated as not-a-time, populate
%     effidx = 1;  % init indices into effort and gaps
%     gapidx = 1;
%     while gapidx <= gapsN
%         % Store currrent block
%         effregions(gapidx,:) = ...
%             [effort(effidx), effort(gaps(gapidx))+effblock_m];
%         effidx = gaps(gapidx)+1; % next start of effort
%         gapidx = gapidx + 1;
%     end
%     % Process last contiguous region
%     effregions(end,:) = [effort(effidx), effort(end)+effblock_m];
%     
%     % NIWC tracks timestamps 
%     effregionsDN = datenum(effregions); % as serial dates
    effregions8601 = dbSerialDateToISO8601(effort);  % as ISO8601
        
    1;
    
    % Pull out track start times
    trkstarts = arrayfun(@(x) x.nest(1).julian_start_time, trk.vTkC);%loops through each element of the array and pulls out the first localization time for each track
    % Can add sorting code if necessary, for now it looks like
    % these are produced sequentially
    assert(issorted(trkstarts), 'Tracks start times are not sorted');%if the condition is false, error is thrown
    
    % Process each region, possibly creating localization records
    trkidx = 1;
    tracksN = length(trk.vTkC);
    for effidx = 1:size(effregions8601, 1)%loop through each effort period
        % Determine tracks within effort
        % We only look at the start of the track for determining which
        % effort it falls within. 
       
        if trkidx > tracksN
            % No more tracks --> none for this effort
            starttrk = [];
            endtrk = [];
        else
            % Move past any track that occurred before effort
            while trkstarts(trkidx) < effort(effidx, 1)
                trkidx = trkidx + 1;
                if trkidx > tracksN
                    break;%break from this loop
                end
            end
            
            % Current track in effort or past it.
            if trkidx <= tracksN && trkstarts(trkidx) < effort(effidx, 2)
                starttrk = trkidx;
                endtrk = starttrk;
                
                % Find any additional tracks in region
                done = trkidx >= tracksN;%false until you get to the very last track in the effort
                while ~ done
                    trkidx = trkidx+1;
                    % include track if in current effort
                    if trkstarts(trkidx) < effort(effidx, 2)
                        endtrk = trkidx;
                        done = trkidx >= tracksN;  % more to check?
                    else
                        done = true;  % outside of effort
                    end
                end
            else
                % no tracks in region
                starttrk = [];
                endtrk = [];
            end
        end
        
        if debug
            fprintf('Effort region %d:  %s - %s\n', ...
                effidx, datestr(effort(effidx,1)), datestr(effort(effidx, 2)));
            if ~ isempty(starttrk)
                fprintf('   %d: ', endtrk - starttrk + 1);
                for idx=starttrk:endtrk
                    fprintf('%s ', datestr(trkstarts(idx)));
                end
                fprintf('\n');
            end
        end
        
        % todo:  How should we populate the instrument metadata
        l = Localize();
        helper.createRequiredElements(l);
        1;
        % todo:  PMRF not really appropriate, not the whole range
        % but a subensemble  Insufficient inforamtion to know what's
        % appropriate here.  
        %
        % Regina, we'll need to work on this to figure out what makes
        % sense for you.  We need to identify this set of effort
        % and links it to a specific data source.  
        % If we use the whole array name as the id along with the effort
        % start time, this could be problematic if we have different
        % efforts on different parts of the array.  Let's discuss this.
       
        ensemble = 'PMRF';
        name = sprintf('%s-%s', ...
            ensemble, dbSerialDateToISO8601(effort(effidx,1)));
        l.setId(name)  % TODO: Determine id
        xmlfname = fullfile(workingdir, sprintf('%s.xml', strrep(name, ':', '_')));
        
        helper.createElement(l, 'Description');
        description = l.getDescription();
        description.setObjectives('Track calling marine mammals across Navy range');
        description.setAbstract('Verified animal tracks');
        description.setMethod('TODO:  high level description');
       
        l.getDataSource().setEnsembleName(ensemble);
        
        algo = l.getAlgorithm();
        algo.setSoftware('TODO:  unknown');
        helper.createElement(algo, 'Version');
        algo.setVersion('TODO: unknown');
        % todo:  Add any parameters needed
        
        
        % Party responsible for the metadata
        responsible = l.getResponsibleParty();
        responsible.setIndividualName('Regina Guazzo');
        responsible.setOrganizationName('Naval Information Warfare Center Pacific');
        % Tethys user
        l.setUserID('rguazzo');
        
        % Effort
        eff = l.getEffort();
        eff.setStart(helper.timestamp(effregions8601{effidx, 1}));
        eff.setEnd(helper.timestamp(effregions8601{effidx, 2}));
        eff.getCoordinateSystem().setType('WGS84');
        
        % Generate localizations within this region of effort
        if ~ isempty(starttrk)
            locations = l.getLocalizations();
            
            % Retrieve the (initially empty list of localization entries
            % and start generating individual entries
            loclist = locations.getLocalization();
            
            for trkidx_save = starttrk:endtrk
                
                %already filtered out bad tracks
%                 if ~ ismember(trk.vTkC(trkidx_save).validation, validationfilter)
%                     % Not one of the tracks that we wanted added
%                     continue  % move on to the next
%                 end
                
                % Create a localization entry for current track
                localization = factory.createLocalizationType();
                
                % Create needed subelements and link into structure
                % Localization->Track
                track = factory.createLocalizationTypeTrack();
                localization.setTrack(track);
                % Localization->Track->WGS84 
                wgs84 = factory.createLongLat3List();
                helper.createRequiredElements(wgs84);
                track.setWGS84(wgs84);
                
                % Add in track points --------------------
                latlongs = vertcat(trk.vTkC(trkidx_save).nest.coordinates);
                startsDN = vertcat(trk.vTkC(trkidx_save).nest.julian_start_time);
                % Massage data into proper formats
                startsISO = dbSerialDateToISO8601(startsDN);
                latlongs(:,2)= rem(latlongs(:,2)+360, 360); % to �East
                
                % Validate to see what's going on...
                delta1 = diff(startsDN);
                if find(delta1 < 0)
                    1;
                end
                delta2 = diff(dbISO8601toSerialDate(startsISO));
                if find(delta2 < 0);
                    1;
                end
                % Regina, I don't see a depth, am I missing it?
                
                % Write the entry
                localization.setTime(helper.timestamp(startsISO{1}));
                longs = wgs84.getLongitudes();
                lats = wgs84.getLatitudes();
                timestamps = wgs84.getTimestamps();
                for posidx = 1:size(latlongs, 1)
                    lats.add(helper.toXsDouble(latlongs(posidx, 1)));
                    longs.add(helper.toXsDouble(latlongs(posidx, 2)));
                    timestamps.add(helper.timestamp(startsISO{posidx}));
                end
                % Find bounding box
                % longitudes can be ambiguous.  We don't know which way
                % around the globe to wrap as our range is [0, 360).  
                % Points that do not lie on a longitude
                % boundary indicate which way we have unless the track
                % goes all the way around the world which is unlikely in
                % our situatin.  When there is no information, we assume
                % the smaller circle.
                smallLong = min(latlongs(:,2));
                bigLong = max(latlongs(:,2));
                if size(latlongs, 1) == 2
                    % Ambiguous, no points outside bounding box,
                    % pick smaller one.
                    if bigLong - smallLong > 180 
                        % Wrapped case is a shorter path
                        west = bigLong;
                        east = smallLong;
                    else
                        west = smallLong;
                        east = bigLong;
                    end
                else
                    long_delta = latlongs(:,2) - smallLong;
                    if find(long_delta < 0, 1, 'first')
                        % At least one point was west of smallLong,
                        % so we wrap the globe
                        west = bigLong;
                        east = smallLong;
                    else
                        % typical case, no wrap around
                        west = smallLong;
                        east = bigLong;
                    end
                end

                
                
                bounds = wgs84.getBounds();
                nw = bounds.getNorthWest();
                nw.setLatitude(max(latlongs(:,1)));
                nw.setLongitude(west);
                
                se = bounds.getSouthEast();
                se.setLatitude(min(latlongs(:,1)));
                se.setLongitude(east);
                % create & add depth if present
                % TODO
                
                % Add to the track list
                loclist.add(localization);
                1;
            end
            
        end
        
        m.marshal(l, xmlfname);
        dbSubmit(q, 'Collection', 'Localizations', 'Overwrite', true, xmlfname);
        1;
        
        
    end
    1;
    
end
    

