% gpl_detections2tethys(server, filenames, OptArgs)
% Given one or more GPL Matlab filenames, import them to Tethys
% Optional arguments
% 'Image', true (default)|false
%    If true, the GPL spectrogram is saved for each detection.  
%    Note that the spectrogram only shows the frequencies that
%    were retained by GPL which are usually those where signal
%    was detected.
function detections = gpl_detections2tethys(server, filenames, varargin)


% Sanity checks for user arguments

if ~isa(server, 'dbxml.Queries')
    error('server must be a Tethys server handle (create with dbInit)');
end

if ischar(filenames)
    filenames = {filenames};  % convert to cell
elseif ~ iscell(filenames)
    error('Expecting cell array of GPL Matlab files to convert');
end

assert(rem(length(varargin), 2) == 0, ...
    'Optional arguments are keyword, value, odd number of arguments');
saveimg = true;  % default
vidx = 1;
while vidx <= length(varargin) 
    switch(varargin{vidx})
        case 'Image'
            saveimg = varargin{vidx+1};
            vidx = vidx + 2;
        otherwise
            error('Unrecognized optional argument');
    end
end

import java.lang.Double;   % todo:  remove me when Helper recompiled
import nilus.MarshalXML;
import nilus.Detections;
import nilus.Helper;
import nilus.AlgorithmType;
import nilus.DetectionEffortKind;
import nilus.SpeciesIDType;
import nilus.CallType;
import nilus.GranularityType;
import nilus.GranularityEnumType;
import nilus.Detection;

debug = false;  % debug mode truncates amount of data processed

m = MarshalXML();
helper = Helper();

tempbase = fullfile(tempdir, 'gpl-tethys');
if ~ isdir(tempbase)
    if ~ mkdir(tempbase)
        fprintf('Reusing existing directory %s', tempbase);
    end
end

for fidx = 1:length(filenames)
    fprintf('Generating Tethys detections from %s\n', filenames{fidx})
    % Find name of file, assume filename starts with range name
    [~, fname , ~] = fileparts(filenames{fidx});
    range = strtok(fname, '_');
    
    detections = Detections();
    % Create the required fields of the top-level of the Detections
    % schema.  Any optional fields will need to be created separately   
    helper.createRequiredElements(detections);
    % Thought about using UUID, but too prone to putting in duplicate
    % documents.
    %uuid = java.util.UUID.randomUUID();
    %detections.setId(uuid.toString());
    detections.setId(fname);

    % Set these fields
    dataSource = detections.getDataSource();
    dataSource.setProject(range);
    dataSource.setDeployment(helper.toXsInteger(1));
    dataSource.setSite(range);
    
    d = load(filenames{fidx});
    
    % Determine and populate effort
    % todo:  replace with appropriate values, just use min/max for now
    start = min(arrayfun(...
        @(h) min([h.detection.calls.julian_start_time]), d.hyd));
    stop = max(arrayfun(...
        @(h) max([h.detection.calls.julian_end_time]), d.hyd));
    effort_8601 = dbSerialDateToISO8601([start; stop]);
    eff = detections.getEffort();
    % Use the nilus.Helper to convert the timestamp string to the 
    % correct type and set it
    eff.setStart(helper.timestamp(effort_8601{1}));
    eff.setEnd(helper.timestamp(effort_8601{2}));
    
    % Effort takes one or more Kinds.  Grab the list
    kinds = eff.getKind();  % list of things we are looking for
    
    % Create the first and only kind of effort that GPL does
    kind = DetectionEffortKind();

    % Label GPL detections as moans (todo:  confirm best choice)
    calltype = nilus.CallType();
    calltype.setValue('moan');
    kind.setCall(calltype);
        
    % GPL detects individual calls as opposed to bins or encounters
    granularitytype = nilus.GranularityEnumType.fromValue('call');
    granularity = nilus.GranularityType();
    granularity.setValue(granularitytype);
    kind.setGranularity(granularity);
    
    % Assume that we are looking for mysticetes
    % As GPL is an energy detector with classification rules afterwards,
    % we may need to customize this based on the post detection
    % classifier.
    % If we are running on a machine that has access to a Tethys server, we
    % could query this, but will hardcode for now as Naval Information
    % Warfare Center Pacific machines (for whom this was developed) may be
    % isolated. 
    
    % Convert serial number to an XsInteger (BigInteger in Java)
    mysticeti = helper.toXsInteger(55298);  % ITIS taxonomic serial number
    speciestype = nilus.SpeciesIDType();
    speciestype.setValue(mysticeti);
    kind.setSpeciesID(speciestype);
    
    kinds.add(kind); % Add to effort list
    
    
    
    % Populate the algorithm
    algo = detections.getAlgorithm();
    helper.createRequiredElements(algo);
    algo.setMethod('Helble, T., Ierley, G. R., D''Spain, G. L., Roch, M. A. and Hildebrand, J. A. (2012). A generalized power-law detection algorithm for humpback whale vocalizations. J. Acous. Soc. Am. 131, 2682-2699.');
    algo.setSoftware('GPL');
    algo.setVersion('todo:  Determine version, encode in detection result?');

    % Set up responsible party?
    
    % Assume detector parameters are the same on all hydrophones.
    % Tyler:  Valid assumption?
    parameters = d.hyd(1).detection.parm;
    append_to = algo.getParameters().getAny();
    % Convert a Matlab struct to individual fields that are
    % added as children of parameters element.
    dbStruct2DOM(parameters, append_to);
    
    % Process the detections on each hydrophone.
    % Detections are organized by phone, use merge sort to put all
    % together.
    
    % remainining flags whether or not any unprocessed detections
    % remain for each hydrophone
    remaining = true(1, length(d.hyd));  % flags for when we are done
    % Next detection to be processed on each hydrophone
    nextidx = ones(length(d.hyd), 1);  % detection index
    nextstarts = Inf(length(d.hyd), 1);  % Will hold detection start time
    callsN = zeros(1, length(d.hyd));  % Number of calls
    
    % More efficient to translate start and end times outside of loop
    fprintf('Converting detection times to ISO8601\n');
    tic
    for hidx=1:length(d.hyd)
        % todo:  find an example of no detections on a phone,
        % might break like this
        callsN(hidx) = length(d.hyd(hidx).detection.calls);
        if callsN(hidx) > 0
            starts{hidx} = dbSerialDateToISO8601(...
                [d.hyd(hidx).detection.calls.julian_start_time]);
            ends{hidx} = dbSerialDateToISO8601(...
                [d.hyd(hidx).detection.calls.julian_end_time]);
            nextstarts(hidx) = d.hyd(hidx).detection.calls(1).start_time;
        else
            remaining(hidx) = false;  % no detections to process
        end
    end
    toc

    % Retrieve the list of systematic on-effort detections (currently
    % empty)
    onEff = detections.getOnEffort();
    detectionList = onEff.getDetection();
    
    % Spectrograms are associated with each detection
    specmaxval = 2^16 - 1;  % Normalizing value for spectrograms
    faxis = linspace(parameters.freq_lo, parameters.freq_hi, ...
        parameters.nfreq);

    % Create a temporary directory to store spectrgorams
    specdir = fullfile(tempbase, [fname, '-image']);
    if ~ mkdir(specdir)
        fprintf('Reusing existing spectrogram directory %s', specdir)
    end
    
    
    if debug
        % Matlab save figure function is slow.  Process a smaller
        % number in debug mode
        callsN = ones(size(callsN)) * 100;
    end
    % merge sort
    fprintf('Merging detections across channels...\n')
    tic;
    count = 0;
    while sum(remaining) > 0
        count = count + 1;
        if rem(count, 500) == 0
            fprintf('%d ', count);
        end
        % Find earliest detection
        [~, h] = min(nextstarts);
        current = nextidx(h);  % current detection on phone h
        
        % Process the detection
        detection = nilus.Detection();
        detection.setStart(helper.timestamp(starts{h}{current}));
        detection.setEnd(helper.timestamp(ends{h}{current}));
        
        % set call, speciesID, channel, noise level and received level
        
        detection.setChannel(helper.toXsInteger(h));
        
        % Parameters is not present by default, create it. 
        % Parameters is a nested class, Detection$Parameters that cannot
        % be handled by normal Matlab/Java syntax.  We could use javaobject
        % to create it, but will instead let the Helper class do it for us.
        helper.createElement(detection, 'Parameters');
        params = detection.getParameters();
        % todo:  use helper when recompiled
        % todo:  received level numbers seem bogus, not sure what spec_rl
        % is
        params.setReceivedLevelDB(...
            Double.valueOf(d.hyd(h).detection.calls(current).spec_rl));
        % todo:  add noise - need to think about this.  Does it make sense
        % to be a separate effort?  How do we note details?
        % Should it be a separate call type 'ambient'?
        params.setQualityAssurance('unverified');
        
        % todo - noise level
        
        
        % Create the spectrogram if requested
        if saveimg
            % Images are saved as attachements, generate & save image
            specname = sprintf('phone-%d_det-%d.png', h, current);
            
            matrix = zeros(d.hyd(h).detection.calls(current).cm_max.size);
            matrix(d.hyd(h).detection.calls(current).cm_max.index) = ...
                d.hyd(h).detection.calls(current).cm_max.values / specmaxval;
            
            taxis = (0:size(matrix, 2)-1) * ...
                parameters.skip / parameters.sample_freq;
            
            figH = figure('visible', 'off');
            imagesc(taxis, faxis, matrix);
            set(gca(figH), 'YDir', 'normal');
            xlabel('sec')
            ylabel('Hz');
            colorbar
            title(sprintf('%s phone %d', starts{h}{current}, h));
            saveas(figH, fullfile(specdir, specname));
            
            % Update the image field so that we can reference the
            % attachment
            % Attachments can be retrieved with a document id and an 
            % image name through the web services interface.  GET the 
            % appropriate URL, e.g. let DETDOC represent the name of the
            % detection .mat file that is being processed without the .mat
            % extension.  If the image field for a detection contains
            % phone-1_det-1.png, and the server was running on TethSrv.dom
            % on port 9779 (default port), then we could retrieve it at:
            % http://TethSrv.dom:9779//Attach/Detections/DETDOC?Image=phone-1_det-1.png
            detection.setImage(specname);            
        end
        
        % append detection to on effort
        detectionList.add(detection);

        idx = nextidx(h);
        idx = idx + 1;
        if (idx > callsN(h))
            % No more calls, make sure this hydrophone is no longer
            % considered.
            remaining(h) = false;
            nextstarts(h) = Inf;
            nextidx(h) = Inf; 
        else
            nextstarts(h) = d.hyd(h).detection.calls(idx).start_time;
            nextidx(h) = idx;
        end
    end
    toc
    1;

    end % hydrophone loop
    
    % File is completed.  Save ot XML (later we'll have it submit
    % automatically).
    m.marshal(detections, sprintf('%s.xml', fname));

end  % files



