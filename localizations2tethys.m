function localizations2tethys(server, arrayfile, locfiles, varargin)
% localizations2tethys(server, arrayfile, locfiles, OptArgs)
% NIWC localization Matlab filenames, import them to Tethys
% It is assumed that localization files have the following file
% pattern:
%   RangeName_localizations_date_*.mat
%   Where RangeName is the range name, e.g. PMRF
%   Have a timestamp in the date position that corresponds to the
%       following format:  DDMonYY_HHMMSS, e.g. 09Feb15_213810
%   and have a corresponding detection file whose name is identical
%   with the exception of detections instead of localizations.
% Sanity checks for user arguments


if ~isa(server, 'dbxml.Queries')
    error('server must be a Tethys server handle (create with dbInit)');
end

if ischar(locfiles)
    locfiles = {locfiles};  % convert to cell
elseif ~ iscell(locfiles)
    error('Expecting cell array of GPL Matlab files to convert');
end

ary = load(arrayfile);
primaries = [ary.array_struct.master];  % primary (master) hydrophones

import java.lang.Double;   % todo:  remove me when Helper recompiled
import nilus.MarshalXML;
import nilus.Localize;
import nilus.Helper;
import nilus.AlgorithmType;
import nilus.LocalizationType;
import nilus.ObjectFactory;

debug = false;  % debug mode truncates amount of data processed

% The following objects help us construct the XML
m = MarshalXML();  % Converts from Java objects to XML
% Auxiliary functions such as converting timestamps, populating elements
helper = Helper();
% Used for creating objects that cannot be easily handled by helper
% or directly invoiked.  Examples include nested classes.  For example,
% the localization CorrelationData element which has a type of
% NIWCCorrelationData can have a list of 
factory = ObjectFactory();

tempbase = fullfile(tempdir, 'gpl-tethys');
if ~ isdir(tempbase)
    if ~ mkdir(tempbase)
        error('Unable to create temporary directory %s', tempbase);
    end
end

fprintf('Generating Tethys XML\n');
for fidx = 1:length(locfiles)
    % Find name of file, assume filename starts with range name
    [~, fname_template , ~] = fileparts(locfiles{fidx});
    range = strtok(fname_template, '_');
    % Find timestamp
    match = regexp(locfiles{fidx}, '\d\d[A-Za-z][A-Za-z][A-Za-z]\d\d_\d\d\d\d\d\d', 'match');
    if isempty(match)
        fprintf('Unable to determine timestamp of %s. Skipping\n', ...
            locfiles{fidx});
        continue
    else
        starttime = datenum(match{1}, 'ddmmmyy_HHMMSS');
    end
    
    % Load localization and detection files
    l = load(locfiles{fidx});
    detfile = strrep(locfiles{fidx}, 'localizations', 'detections'); 
    d = load(detfile);
    % todo:  turn image back on, off now for speed
    %detections = gpl_detections2tethys(server, detfile, 'Image', false);
    
    % For each primary hydrophone in an array
    for arridx = 1:length(primaries)
        hydidx = primaries(arridx);  % note primary hydrophone id
        
        % Hydrophones associated with primary hydrophone
        secondaries = ary.array_struct(arridx).slave;
        
        fprintf('Primary phone %d, secondaries: ', hydidx)
        fprintf('%d ', secondaries);
        fprintf('from file %s\n', locfiles{fidx});

        % Start new Localize document for this set of localizations
        localizations = Localize();
        id = sprintf('%s_primary%d', fname_template, hydidx);
        localizations.setId(id);
        fname = sprintf('%s.xml', id);
        
        % Create the required fields of the top-level of the Localizations
        % schema.  Any optional fields will need to be created separately
        helper.createRequiredElements(localizations);
        
        dataSource = localizations.getDataSource();
        dataSource.setProject(range);
        dataSource.setDeployment(helper.toXsInteger(1));
        dataSource.setSite(range);

        % Populate the algorithm
        algo = localizations.getAlgorithm();
        helper.createRequiredElements(algo);
        algo.setMethod('Helble, T., Need reference');
        algo.setSoftware('Need Name');
        algo.setVersion('todo:  Determine version, encode in detection result?');

        % Assume detector parameters are the same on all hydrophones.
        % Tyler:  Valid assumption?
        parameters = l.localize_struct.parm;
        append_to = algo.getParameters().getAny();

        dbStruct2DOM(parameters, append_to);

        % Set up party responsible for metadata?
        helper.createElement(localizations, 'ResponsibleParty');
        responsible_party = localizations.getResponsibleParty();
        responsible_party.setOrganizationName('Naval Information Warfare Center');
        responsible_party.setIndividualName('Regina Guazzo');

        % User identifier (hardcoded for now)
        localizations.setUserID('rguazzo');
        
        effort = localizations.getEffort();
        coordsys = effort.getCoordinateSystem();
        coordsys.setType('WGS84'); % world geodetic system 1984

        % todo:  We need to know the effort more accuractely
        % For now, we use the earliest and latest dates on 
        % the hydrophone of interest.  
        start = min([d.hyd(hydidx).detection.calls.julian_start_time]);
        stop = max([d.hyd(hydidx).detection.calls.julian_end_time]);
        effort_8601 = dbSerialDateToISO8601([start; stop]);
        effort.setStart(helper.timestamp(effort_8601{1}));
        effort.setEnd(helper.timestamp(effort_8601{2}));
        
        
        % Create the set of documents to which these localizations refer
        % todo:  Use detections to determine appropriate document
        norefs = true;
        if norefs
            fprintf('Not creating ReferencedDocuments\n');
        else
            helper.createElement(effort, 'ReferencedDocuments');
            refdocs = effort.getReferencedDocuments();        
            doclist = refdocs.getDocument();  % get empty list and add to it
        end
        
        % Store correlation data associated with these localizations
        helper.createElement(localizations, 'IntermediateData');
        intermediate = localizations.getIntermediateData();
        % Create Correlations, and create an empty list of Correlatin
        % entries
        helper.createElement(intermediate, 'Correlations');
        correlations = intermediate.getCorrelations();
        helper.createElement(correlations, 'Correlation');
        correlation_list = correlations.getCorrelation();
        for sidx = 1:length(secondaries)
            % Create a correlation entries, populate, and add to list
            correlation = factory.createNIWCCorrelationDataCorrelation();
            correlation.setPrimary(helper.toXsInteger(hydidx));
            correlation.setSecondary(helper.toXsInteger(secondaries(sidx)));
            % Convert matrix to string, add newlines to prevent long lines
            corrstr = mat2str(l.localize_struct.hyd(hydidx).cc_matrix{sidx});
            corrstrn = strrep(corrstr, ';', '\n');
            correlation.setCorrelations(corrstrn);
            correlation_list.add(correlation);
        end
                
        
       
        
        % 
        1;
        
        
        % TODO:  Query Tethys server to get sample rate.
        Fs = 96000;  % for now.  Regina & Tyler, is this right, or 96 kHz
        
        if ~ isempty(l.localize_struct.hyd(hydidx).coord_time)
            % Compute arrival times at primary hydrophones
            % Faster to do outside of loop
            samples = l.localize_struct.hyd(hydidx).coord_time(:,1);
            offset_s = samples / Fs;
            offset_d = offset_s / (24*3600);
            firstarrivals = starttime + offset_d;
            firstarrivals_iso8601 = dbSerialDateToISO8601(firstarrivals);
            if ~ iscell(firstarrivals_iso8601)
                firstarrivals_iso8601 = {firstarrivals_iso8601};
            end
            % Process localizations, get localization list and add to it
            tmp = localizations.getLocalizations();
            posits = tmp.getLocalization();
            for lidx = 1:size(l.localize_struct.hyd(hydidx).coordinates, 2)
                % Create new localization information
                % NIWC uses -/+ degrees east, we always use positive degrees
                % east
                posit = LocalizationType();
                
                % Time of first arrival
                first = helper.timestamp(firstarrivals_iso8601{lidx});
                posit.setTime(first);
                

                % Get lat long and depth
                helper.createElement(posit, 'WGS84');
                location = posit.getWGS84();
                helper.createRequiredElements(location);
                lld = l.localize_struct.hyd(hydidx).coordinates(:,lidx);
                location.setLatitude(lld(1))
                long = lld(2);
                if long < 0, long = 360.0 + long; end  % degrees east
                location.setLongitude(long);
                location.setElevationM(-lld(3));
                location.setDepthM(lld(3));
                
                posits.add(posit);
            end
            
        end
        m.marshal(localizations, fname);
        
        1;
    end
end
    
    
